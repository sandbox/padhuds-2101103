
/**
 *  @file
 *  README for the Domain Specific Google Analytics.
 */

**Description:
The Domain Navigation module provides support functions for google analytics on domain specific.

For a full description of the module, visit the project page:
  http://drupal.org/project/domain_google_analytics

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/domain_google_analytics
  
-- REQUIREMENTS --

Domain Access Module (http://drupal.org/project/domain)

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Once the module is installed navigate to the domain list (admin/build/domain/view) and click on the "Google Analytics" link for each domain to add google analytics code and web property id.